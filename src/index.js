import { getNavBarItems } from "./api/api";
import { createContainer, setupContainer } from "./components/body/body";
import { setupFooter } from "./components/footer/footer";
import { setupHeader } from "./components/header/header";
import "./style.css";

setupHeader(document.getElementById("app"), {
  navBarItems: getNavBarItems(),
});

setupContainer(document.getElementById("app"));

setupFooter(document.getElementById("app"));
