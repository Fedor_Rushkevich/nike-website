export const images = {
  navBarImg: [
    {
      id: "0001",
      alt: "twitter",
      src: "./assets/icons/twitter.svg",
    },
    {
      id: "0002",
      alt: "instagram",
      src: "./assets/icons/instagram.svg",
    },
    {
      id: "0003",
      alt: "facebook",
      src: "./assets/icons/facebook.svg",
    },
  ],
  logo: [{ id: "0004", alt: "logo", src: "./assets/logo/logo.svg" }],
  buttons: [
    {
      id: "0005",
      alt: "button-left",
      src: "./assets/icons/arrow-left.svg",
    },
    {
      id: "0006",
      alt: "button-right",
      src: "./assets/icons/arrow-right.svg",
    },
  ],
  mainContent: [
    {
      id: "0007",
      alt: "sneaker-1",
      src: "./assets/main-content/sneaker-1.svg",
    },
    {
      id: "0008",
      alt: "sneaker-2",
      src: "./assets/main-content/sneaker-2.svg",
    },
    {
      id: "0009",
      alt: "sneaker-3",
      src: "./assets/main-content/sneaker-3.svg",
    },
    {
      id: "0010",
      alt: "sneaker-4",
      src: "./assets/main-content/sneaker-4.svg",
    },
    {
      id: "0011",
      alt: "shadow",
      src: "./assets/main-content/shadow.svg",
    },
  ],
  stars: [
    {
      id: "0012",
      alt: "star",
      src: "./assets/icons/white-star.svg",
    },
    {
      id: "0013",
      alt: "nostar",
      src: "./assets/icons/empty-star.svg",
    },
  ],
};
