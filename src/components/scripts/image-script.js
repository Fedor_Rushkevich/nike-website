export function ImageScript(images, className, imageId, imageSection) {
  const element = document.createElement("img");

  element.className = className;

  let arrForSrch;

  for (let key in images) {
    if (Array.isArray(images[key]) && key == imageSection) {
      arrForSrch = images[key];
    }
  }

  const img = arrForSrch.find((element) => element.id === imageId);

  element.src = img.src;
  element.alt = img.alt;

  return element;
}
