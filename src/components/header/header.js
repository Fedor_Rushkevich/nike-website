import { images } from "../../api/images";
// import { ImageScript } from "../scripts/image-script";
import "./header.css";

export function setupHeader(parent, data) {
  //create header

  const header = createHeader(data);

  parent.append(header);
}

function createHeader(data) {
  const header = document.createElement("header");

  header.className = "header";

  const navBar = createNavBar(data.navBarItems);

  header.append(navBar);

  return header;
}

function createNavBar(items) {
  const divLogo = document.createElement("div");

  const TextDiv = document.createElement("div");

  const TextDivTwo = document.createElement("div");

  const divIcons = document.createElement("div");

  const navBar = document.createElement("navBar");

  const ul = document.createElement("ul");

  const logo = document.createElement("img");

  const NavBarUl = document.createElement("ul");

  logo.className = "header__navBar__logo";
  logo.src = images.logo[0].src;
  logo.alt = images.logo[0].alt;

  navBar.className = "navBar";
  divLogo.append(logo);

  ul.className = "header__navBar-list";
  const arr = images.navBarImg;

  arr.forEach((image) => {
    const li = document.createElement("li");

    const img = document.createElement("img");

    img.alt = image.alt;
    img.src = image.src;

    li.append(img);
    NavBarUl.append(li);
  });

  const spanInOne = document.createElement("span");

  spanInOne.innerHTML = items[0].title;
  spanInOne.className = "header__navBar__textOne";
  const spanInTwo = document.createElement("span");

  spanInTwo.innerHTML = items[1].title;
  spanInTwo.className = "header__navBar__textTwo";

  TextDiv.append(spanInOne);
  TextDivTwo.append(spanInTwo);

  TextDiv.className = "header__navBar__span-text";
  TextDivTwo.className = "header__navBar__span-textTwo";

  divLogo.className = "header__navBar__img";
  divIcons.className = "header__navBar__icons";

  navBar.append(divLogo);
  navBar.append(TextDiv);
  navBar.append(divIcons);
  divIcons.append(NavBarUl);
  navBar.append(TextDivTwo);
  divLogo.append(logo);

  return navBar;
}
