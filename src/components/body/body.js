import { images } from "../../api/images";
import { ImageScript } from "../scripts/image-script";
import "./body.css";

export function setupContainer(parent) {
  const container = createContainer();

  parent.append(container);
}

export function createContainer() {
  const container = document.createElement("div");

  container.className = "body__container";

  const mainContent = CreateMainContent();
  container.append(mainContent);

  return container;
}

function CreateMainContent() {
  const div = document.createElement("div");

  div.className = "container__mainContent__div";

  const shadow = ImageScript(
    images,
    "container__mainContent__shadow",
    "0011",
    "mainContent"
  );

  const sneakerText = document.createElement("div");

  sneakerText.className = "conatiner__mainContent__text";
  sneakerText.innerHTML = "NIKE MAG <br>BACK TO THE FUTURE";

  div.append(sneakerText);
  div.append(shadow);

  const buttonDiv = document.createElement("div");

  buttonDiv.className = "container__mainContent__button";

  const arrow = ImageScript(
    images,
    "container__mainContent__arrow",
    "0005",
    "buttons"
  );

  buttonDiv.append(arrow);

  const circle = document.createElement("div");

  circle.className = "container_mainContent__circle";

  buttonDiv.append(circle);

  //   ----------------------------------------------

  const buttonDivTwo = document.createElement("div");

  buttonDiv.className = "container__mainContent__buttonTwo";

  const arrowTwo = ImageScript(
    images,
    "container__mainContent__arrow-r",
    "0006",
    "buttons"
  );

  buttonDiv.append(arrowTwo);

  const circleTwo = document.createElement("div");

  circleTwo.className = "container_mainContent__circleTwo";

  buttonDivTwo.append(circleTwo);

  buttonDivTwo.className = "container__mainContent__buttonTwo";
  div.append(buttonDiv);
  div.append(buttonDivTwo);

  return div;
}
