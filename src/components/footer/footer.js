import { images } from "../../api/images";
import { ImageScript } from "../scripts/image-script";
import "./footer.css";

export function setupFooter(parent) {
  const footer = createFooter();

  parent.append(footer);
}

function createFooter() {
  const container = document.createElement("div");

  container.className = "footer__container";

  const price = document.createElement("div");

  price.innerHTML = "$749";

  price.className = "footer__container__price";

  container.append(price);

  const stars = document.createElement("div");

  stars.className = "footer__container__stars";

  for (let i = 0; i < 5; i++) {
    if (i < 4) {
      const star = ImageScript(
        images,
        "footer__container__star",
        "0012",
        "stars"
      );
      stars.append(star);
    } else {
      const star = ImageScript(
        images,
        "footer__container__emty-star",
        "0013",
        "stars"
      );
      stars.append(star);
    }
  }

  container.append(stars);

  const footerText = document.createElement("div");

  footerText.className = "footer__container__text";

  footerText.innerHTML =
    "A Woocommerce product gallery Slider for Slider <br>Revolution with mind-blowing visuals.";

  container.append(footerText);

  const buyButton = document.createElement("div");

  buyButton.className = "footer__container__button";

  const buy = document.createElement("div");

  buy.className = "footer__container__buy";

  buy.innerHTML = "BUY NOW";

  buyButton.append(buy);

  const line = document.createElement("div");

  line.className = "footer__container__line";

  buyButton.append(line);
  container.append(buyButton);

  return container;
}
